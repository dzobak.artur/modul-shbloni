﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




// Singltone/
// Тому що в умові задачі було сказано що об'єкт історії повинен існувати в єдиному екземплярі.
// Використано Instance

namespace modul15
{
    public class Payment
    {
        public decimal Amount { get; }
        public DateTime Date { get; }

        public Payment(decimal amount, DateTime date)
        {
            Amount = amount;
            Date = date;
        }
    }
    public class PaymentHistory
    {
        private static PaymentHistory instance;
        private List<Payment> payments;

        private PaymentHistory()
        {
            payments = new List<Payment>();
        }
        public static PaymentHistory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PaymentHistory();
                }
                return instance;
            }
        }
        public void RecordPayment(decimal amount, DateTime date)
        {
            Payment payment = new Payment(amount, date);
            payments.Add(payment);
        }
        public void DisplayPayments()
        {
            Console.WriteLine("Payment History:");
            payments.ForEach(payment =>
            {
                Console.WriteLine($"Date: {payment.Date}, Amount: {payment.Amount:C}");
            });
        }
        public decimal TotalPayments()
        {
            decimal total = payments.Sum(payment => payment.Amount);
            return total;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            PaymentHistory history = PaymentHistory.Instance;
            history.RecordPayment(100.00m, DateTime.Now);
            history.RecordPayment(150.00m, DateTime.Now.AddDays(-1));
            history.RecordPayment(75.50m, DateTime.Now.AddDays(-2));
            history.DisplayPayments();


            decimal total = history.TotalPayments();
            Console.WriteLine($"Total Payments: {total:C}");
        } 
    }
}
